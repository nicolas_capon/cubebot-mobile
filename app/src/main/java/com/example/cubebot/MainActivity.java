package com.example.cubebot;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {

    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter writingTagFilters[];
    Tag myTag;
    Context context;
    TextView nfc_contents;
    Button media_record_button;
    TextInputEditText sound_url;
    private MediaPlayer mediaPlayer;
    private MediaPlayer mediaPlayerPreview;
    private MediaRecorder mediaRecorder = null;
    private String mediaFilename;
    private String api_url;
    private String card_id;
    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((R.layout.activity_main));
        // Instantiate variables
        this.api_url = "http://serveurnico.synology.me/card";// "http://rpi3/card"; //
        mediaFilename = getExternalCacheDir().getAbsolutePath() + "/micro.mp3";
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayerPreview = new MediaPlayer();
        nfc_contents = (TextView) findViewById(R.id.nfc_contents);
        sound_url = (TextInputEditText) findViewById(R.id.sound_url);
        context = this;
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 0);
        if (nfcAdapter == null) {
            toast.makeText(this, "This device does not support NFC", Toast.LENGTH_LONG).show();
            finish();
        }
        readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writingTagFilters = new IntentFilter[]{tagDetected};
        media_record_button = (Button) findViewById(R.id.media_record_button);
        media_record_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    startRecording();
                    Toast.makeText(context, "Hold to record", Toast.LENGTH_LONG);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        stopRecording();
                    } catch (RuntimeException e){
                        // If button is spammed
                        System.out.println(e);
                    }
                    Toast.makeText(context, "Recorded", Toast.LENGTH_LONG);
                }
                return false;
            }
        });
        // Button to test recorded sound
        Button testButton = (Button) findViewById(R.id.test_button);
        testButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    File recordedFile = new File(mediaFilename);
                    if (recordedFile.exists()) {
                        new SoundPlayer(mediaPlayerPreview, flag).play(mediaFilename);
                    } else {
                        System.out.println("No recorded file");
                    }
                }
                return false;
            }
        });
        Button saveButton = (Button) findViewById(R.id.save_button);
        saveButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    String endpoint = api_url + "/" + card_id;
                    String url = sound_url.getText().toString();
                    if (url.equals("")){
                        // upload sound file
                        System.out.println("send sound: " + url);
                        API api = new API(endpoint, mediaFilename, "sound");
                        api.execute();
                    } else {
                        // upload url of a sound file
                        System.out.println(url);
                        API api = new API(endpoint, url, "url");
                        api.execute();
                    }
                };
                return false;
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 0);
        }
    }

    private void readFromIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            System.out.println(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
            Tag myTag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            System.out.println(convertByteArrayToHexString(myTag.getId()));
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            try {
                buildTagViews(msgs);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String convertByteArrayToHexString(byte[] b) {
        // TODO: Remove ? only useful to print tag id in hew format
        if (b != null) {
            StringBuilder s = new StringBuilder(2 * b.length);
            for (int i = 0; i < b.length; ++i) {
                final String t = Integer.toHexString(b[i]);
                final int l = t.length();
                if (l > 2) {
                    s.append(t.substring(l - 2));
                } else {
                    if (l == 1) {
                        s.append("0");
                    }
                    s.append(t);
                }
            }
            return s.toString();
        } else {
            return "";
        }
    }

    private void buildTagViews(NdefMessage[] msgs) throws IOException {
        if (msgs == null || msgs.length == 0 && flag == true) {
            return;
        }
        flag = true;
        String text = "";
        // String tagId = new String(msgs[0].getRecords()[0].getType());
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get text encoding
        int languageCodeLength = payload[0] & 0063; // Get the Language code, e.g: "en"
        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
        try {
            // Get the text
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("UnsupportedEncoding", e.toString());
        }

        if (text.contains("scryfall.com/cards/")){
            // Extract card ID
            this.card_id = text.substring(text.lastIndexOf('/') + 1);
            // Get image and sound for this card
            ImageView imageView = (ImageView) findViewById(R.id.imageView);
            new ImageLoader(api_url + "/image/" + card_id, imageView).execute();
            new SoundPlayer(mediaPlayer, flag).play(api_url + "/sound/" + this.card_id);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        readFromIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
    }

    private void startRecording() {
        // Remove text from the URL textView first
        TextInputEditText editTextURL = (TextInputEditText) findViewById(R.id.sound_url);
        editTextURL.setText("");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
            // Record to the external cache directory for visibility
            mediaRecorder.setOutputFile(mediaFilename);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

            try {
                mediaRecorder.prepare();
            } catch (IOException e) {
                System.out.println("prepare() failed");
                System.out.println(e.toString());
                // Log.e(LOG_TAG, "prepare() failed");
            }

            mediaRecorder.start();

            // Log.e(LOG_TAG, "Recording:Calling ");
            System.out.println("Recording: Calling ");
        }

    }

    private void stopRecording() {
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        // if we are using MediaRecorder, release it first
        releaseMediaRecorder();
    }

    private void releaseMediaRecorder() {
        // When application goes in background
        if (mediaRecorder != null) {
            // clear recorder configuration
            mediaRecorder.reset();
            // release the recorder object
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity, activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, 0);
        adapter.enableForegroundDispatch(activity, pendingIntent, null, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForegroundDispatch(this, nfcAdapter);
    }
}