package com.example.cubebot;

import android.os.AsyncTask;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class API extends AsyncTask<String, String, Void> {

    private String url;
    private String data;
    private String type;

    public API(String url, String data, String type) {
        //set context variables if required
        this.url = url;
        this.data = data;
        this.type = type;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(String... params) {
        switch (type){
            case "sound":
                try {
                    uploadSound();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "url":
                try {
                    uploadString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        return null;
    }

    public void uploadString() throws IOException {
        final String end = "\r\n";
        final String twoHyphens = "--";
        final String boundary = "*****++++++************++++++++++++";

        URL url = null;
        try {
            url = new URL(this.url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        try {
            conn.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        /* setRequestProperty */
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Charset", "UTF-8");
        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

        DataOutputStream ds = null;
        try {
            ds = new DataOutputStream(conn.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ds.writeBytes(twoHyphens + boundary + end);
        ds.writeBytes("Content-Disposition: form-data; name=\"url\"" + end + end + data + end);
        ds.writeBytes(end);

        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        int length = -1;

        ds.writeBytes(end);
        ds.writeBytes(twoHyphens + boundary + twoHyphens + end);
        /* close streams */
        ds.flush();
        ds.close();

        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
            System.out.println("POST URL [" + this.data + "] Request OK");
        } else {
            System.out.println("POST URL [" + this.data + "] Request FAILED");
        }
    }

    public void uploadSound() throws IOException {
        File sound = new File(this.data);
        final String end = "\r\n";
        final String twoHyphens = "--";
        final String boundary = "*****++++++************++++++++++++";

        URL url = null;
        try {
            url = new URL(this.url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        try {
            conn.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        /* setRequestProperty */
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Charset", "UTF-8");
        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

        DataOutputStream ds = null;
        try {
            ds = new DataOutputStream(conn.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ds.writeBytes(twoHyphens + boundary + end);
        ds.writeBytes("Content-Disposition: form-data; name=\"from\"" + end + end + "auto" + end);
        ds.writeBytes(twoHyphens + boundary + end);
        ds.writeBytes("Content-Disposition: form-data; name=\"to\"" + end + end + "ja" + end);
        ds.writeBytes(twoHyphens + boundary + end);
        ds.writeBytes("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + sound + "\"" + end);
        ds.writeBytes(end);

        FileInputStream fStream = new FileInputStream(sound);
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        int length = -1;

        while ((length = fStream.read(buffer)) != -1) {
            ds.write(buffer, 0, length);
        }
        ds.writeBytes(end);
        ds.writeBytes(twoHyphens + boundary + twoHyphens + end);
        /* close streams */
        fStream.close();
        ds.flush();
        ds.close();

        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
            System.out.println("POST SOUND Request OK");
        } else {
            System.out.println("POST SOUND Request FAILED");
        }
    }
}
