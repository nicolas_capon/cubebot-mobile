package com.example.cubebot;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;

import java.io.IOException;

public class SoundPlayer extends AsyncTask<Void, Void, Bitmap> {

    private String path;
    private MediaPlayer mediaPlayer;
    private boolean flag;
    private boolean state;

    public SoundPlayer(MediaPlayer mediaPlayer, boolean flag) {
        this.mediaPlayer = mediaPlayer;
        this.flag = flag;
        this.state = true;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
            this.state = false;
        } catch (IllegalStateException e) {
           e.printStackTrace();
           this.state = false;
        } catch (RuntimeException e) {
            e.printStackTrace();
            this.state = false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        boolean isPlaying = false;
        try {
            isPlaying = mediaPlayer.isPlaying();
        } catch (IllegalStateException e){
            e.printStackTrace();
        }
        if (!isPlaying){
            mediaPlayer.release();
        }
        this.flag = !flag;
    }

    public void play(String path){
        this.path = path;
        boolean isPlaying = false;
        if (state){
            try {
                isPlaying = mediaPlayer.isPlaying();
            } catch (IllegalStateException e){
                e.printStackTrace();
            }
            if (!isPlaying){
                this.execute();
            }
        }
    }
}